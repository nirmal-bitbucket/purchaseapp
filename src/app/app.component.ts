import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';

import firebase from 'firebase';




@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {

    firebase.initializeApp({
      apiKey: "AIzaSyCLYnUA7TCScNbajzxB6syj3nsYBhOz55Q",
      authDomain: "purchase-app-dce72.firebaseapp.com",
      databaseURL: "https://purchase-app-dce72.firebaseio.com",
      projectId: "purchase-app-dce72",
      storageBucket: "purchase-app-dce72.appspot.com",
      messagingSenderId: "822606763372"
    });


    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
