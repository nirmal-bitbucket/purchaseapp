import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HttpModule } from '@angular/http';

import  { BookingPage } from '../pages/booking/booking';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage} from '../pages/login/login';
import { DetailsPage } from '../pages/details/details';
import  {RecievingPage } from '../pages/recieving/recieving';
import { BookDetPage } from '../pages/book-det/book-det';
import { RecieveDetPage } from '../pages/recieve-det/recieve-det';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BookingProvider } from '../providers/booking/booking';


@NgModule({
  declarations: [
    MyApp,
    BookingPage,
  
    TabsPage,
    LoginPage,
    DetailsPage,
    RecievingPage,
    BookDetPage,
    RecieveDetPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BookingPage,
    TabsPage,

    LoginPage,
    DetailsPage,
    RecievingPage,
    BookDetPage,
    RecieveDetPage   
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BookingProvider
  ]
})
export class AppModule {}
