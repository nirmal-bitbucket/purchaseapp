import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import firebase from 'firebase';


/*
  Generated class for the BookingProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BookingProvider {
  private po:any;
  private dc:any;
  private style:string;
  private department:string;

  constructor(public http: Http) {
        
    

  }
  public view(){
    var rootRef=firebase.database().ref().child("users");
    
          rootRef.on("child_added",snap=>{
            var po = snap.child("PO").val();
            this.po=po;
    
            var style =snap.child("Style").val();
            this.style=style;
    
            var dep =snap.child("Department").val();
            this.department=dep;
    
            var dc=snap.child("DC").val();
            this.dc=dc;
          })      
  }
}
