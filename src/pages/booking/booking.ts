//import { BookingProvider } from './../../providers/booking/booking';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BookDetPage } from '../../pages/book-det/book-det';
import { Http } from '@angular/http';
import  firebase   from 'firebase';



/**
 * Generated class for the BookingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-booking',
  templateUrl: 'booking.html',
  //providers:[BookingProvider]
})
export class BookingPage {

   private po:any;
   private dc:any;
   private style:string;
   private department:string;
  // //data : any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    var rootRef=firebase.database().ref().
    child("users");

      rootRef.on("child_added",snap=>{
        var po = snap.child("PO").val();
        this.po=po;

        var style =snap.child("Style").val();
        this.style=style;

        var dep =snap.child("Department").val();
        this.department=dep;

        var dc=snap.child("DC").val();
        this.dc=dc;
      })



  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad BookingPage');
  // }
   details(){
    this.navCtrl.push(BookDetPage);
   }
  // getdata() {
  //   this.http.get('assets/booking-tab.json').map((res) => res.json())
  //     .subscribe(data => {
  //       // this.po=data.item1.po;
  //       // this.dc=data.dc;
  //       // this.style=data.style;
  //       // this.department=data.department;
  //       console.log(data);
  //       this.data = data.items;

  //     })
  // }

    
  


  


}
