import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecieveDetPage } from './recieve-det';

@NgModule({
  declarations: [
    RecieveDetPage,
  ],
  imports: [
    IonicPageModule.forChild(RecieveDetPage),
  ],
})
export class RecieveDetPageModule {}
