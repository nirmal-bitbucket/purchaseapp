import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DetailsPage } from '../../pages/details/details';
import { Http } from '@angular/http';
/**
 * Generated class for the RecieveDetPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recieve-det',
  templateUrl: 'recieve-det.html',
})
export class RecieveDetPage {
  po:any;
  style:string;
  department:string;
  po_qty:any
  highest_var:any;
  grv_qty:any;
  booked_qty:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http:Http) {
    this.getdata();
  }

  getdata() {
    this.http.get('assets/recieve-var-det.json').map((res) => res.json())
      .subscribe(data => {
          this.po=data.po;
          this.style=data.style;
          this.department=data.department;
          this.po_qty=data.po_qty;
          this.highest_var=data.highest_var;
          this.grv_qty=data.grv_qty;
          this.booked_qty=data.booked_qty;
      })
  }
  view_details(){
    this.navCtrl.push(DetailsPage);
  }

}
