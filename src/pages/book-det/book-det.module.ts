import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookDetPage } from './book-det';

@NgModule({
  declarations: [
    BookDetPage,
  ],
  imports: [
    IonicPageModule.forChild(BookDetPage),
  ],
})
export class BookDetPageModule {}
