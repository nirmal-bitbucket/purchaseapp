import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DetailsPage } from  '../../pages/details/details';

import { Http } from '@angular/http';
/**
 * Generated class for the BookDetPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-book-det',
  templateUrl: 'book-det.html',
})
export class BookDetPage {

  po:any;
  style:string;
  department:string;
  dc_date:string;
  days:string;
  book_date:string;
  highest_var:string;
  exp_qty:any;
  booked_qty:any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public http:Http) {
    this.getdata();
  }

  getdata() {
    this.http.get('assets/book-var-det.json').map((res) => res.json())
      .subscribe(data => {
          this.po=data.po;
          this.style=data.style;
          this.department=data.department;
          this.dc_date=data.dc_date;
          this.days=data.days;
          this.book_date=data.book_date;
          this.highest_var=data.highest_var;
          this.exp_qty=data.exp_qty;
          this.booked_qty=data.booked_qty;
      })
  }
  view_details(){
    this.navCtrl.push(DetailsPage);
  }

}
