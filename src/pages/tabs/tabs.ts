import { Component } from '@angular/core';

import { BookingPage } from '../booking/booking';
//import  { ReturnPage} from '../return/return';
import { RecievingPage } from '../recieving/recieving';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = BookingPage;
  tab2Root = RecievingPage;

  constructor() {

  }
}
