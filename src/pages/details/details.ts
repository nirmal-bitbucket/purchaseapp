import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/**
 * Generated class for the DetailsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  sku : any;
  variance:any;
  size:any;
  color:any;
  exp_qty:any;
  act_qty:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    this.getdata();
  }
  getdata() {
    this.http.get('assets/details.json').map((res) => res.json())
      .subscribe(data => {
        console.log(data)
        this.sku = data.sku;
        this.variance=data.variance;
        this.size=data.size;
        this.color=data.color;
        this.exp_qty=data.exp_qty;
        this.act_qty=data.act_qty;
      })
  }


}
