import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecievingPage } from './recieving';

@NgModule({
  declarations: [
    RecievingPage,
  ],
  imports: [
    IonicPageModule.forChild(RecievingPage),
  ],
})
export class RecievingPageModule {}
