import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RecieveDetPage } from  '../../pages/recieve-det/recieve-det';
import { Http } from '@angular/http';
/**
 * Generated class for the RecievingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recieving',
  templateUrl: 'recieving.html',
})
export class RecievingPage {

  po:any;
  dc:any;
  style:string;
  department:string;
  data : any = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http:Http) {
    this.getdata();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecievingPage');
  }
  details(){
    this.navCtrl.push(RecieveDetPage);
  }
  getdata() {
    this.http.get('assets/booking-tab.json').map((res) => res.json())
      .subscribe(data => {
        // this.po=data.item1.po;
        // this.dc=data.dc;
        // this.style=data.style;
        // this.department=data.department;
        console.log(data);
        this.data = data.items;

      })
  }

}
